<!doctype html>
<html lang="en">
  <head>

    @include('layouts.header')
  
  </head>
  <body>

    @yield('content')
  
    @include('layouts.script')

    @yield('script')
  
  </body>
</html>