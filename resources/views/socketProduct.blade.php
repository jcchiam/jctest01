@extends('layouts.master')

@section('content')

<div class="container">
  <h1 class="text-center mt-5 mb-5">Voting {{ $product->name }}</h1>
  <input hidden id="product" value="{{ $product->slug }}">
  <div class="row text-center d-flex align-items-center">
    <div class="col-3">
      @if($vote == true)
        <div class="voteButton">
          <a type="button" class="btn btn-primary vote mb-2" actionType="like">Like</a>
        </div>
      @endif
      <div class="result  @if($vote == true) d-none @endif " id="likePercent">
        <!-- <span id="likePercent"> 0 </span> <span>%</span> -->
      </div>
    </div>
    <div class="col ">
      <img src="{{ asset($product->img) }}" class="img-fluid w-50">
    </div>
    <div class="col-3 ">
       @if($vote == true)
        <div class="voteButton">
          <a type="button" class="btn btn-primary vote mb-2" actionType="dislike">Dislike</a>
        </div>
      @endif
      <div  class="result   @if($vote == true) d-none @endif" id="dislikePercent">
        <!-- <span id="dislikePercent"> 0 </span> <span>%</span> -->
      </div>
    </div>
  </div>
</div>


@endsection


@section('script')

<script src="https://cdn.socket.io/4.1.2/socket.io.min.js" integrity="sha384-toS6mmwu70G0fw54EGlWWeA4z3dyJ+dlXBtSURSKN4vyRFOcxd3Bzjj/AoOwY+Rg" crossorigin="anonymous"></script>

<script>
  $(document).on('click', "a.vote", function() {

    let type = $(this).attr("actionType");
    $.ajax({
      url: "{{ route('api.product.vote') }}",
      method: "POST",
      dataType: 'json',
      data: {
        _token: "{{ csrf_token() }}",
        product: "{{ $product->slug }}",
        type: type , 
        ipaddress : "{{ $ipAddress }}"
      }
    }).done(function(response) {
      if (response.status) {
        $("#likePercent").html(response.data.like);
        $("#dislikePercent").html(response.data.dislike);
        $(".voteButton").addClass('d-none');
        $(".result").removeClass('d-none');

      } else {
        alert(response.message);
      }

    })
  });


  let domain = '{{ Request::getHost() }}';
  let socket_port = '3000';
  let socket = io(domain + ':' + socket_port);
  let product = {
                  product : $("#product").val() ,
                  ipaddress : "{{ $ipAddress }}" , 

                };
  var testConnection = socket.on('connection');
 
  async function myfunction() {

    await socket.emit('getProductVoteResult', product);
    await socket.on('showProductVoteResult', (product) => {
      
      $("#likePercent").html(product['like']);
      $("#dislikePercent").html(product['dislike']);
      
    });
    
  }
  
  setInterval(myfunction, '1500');
</script>




@endsection