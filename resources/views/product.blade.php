@extends('layouts.master')

@section('content')

<div class="container">
  <h1 class="text-center mt-5 mb-5">Voting {{ $product->name }}</h1>
  <div class="row text-center d-flex align-items-center">
    <div class="col-3">
      <div class="voteButton">
        <a type="button" class="btn btn-primary vote mb-2" actionType="like">Like</a>
      </div>
      <div class="result" id="likePercent">
        <!-- <span id="likePercent"> 0 </span> <span>%</span> -->
      </div>
    </div>
    <div class="col ">
      <img src="{{ asset($product->img) }}" class="img-fluid w-50">
    </div>
    <div class="col-3 ">
      <div class="voteButton" >
        <a type="button" class="btn btn-primary vote mb-2" actionType="dislike">Dislike</a>
      </div>
      <div  class="result" id="dislikePercent">
        <!-- <span id="dislikePercent"> 0 </span> <span>%</span> -->
      </div>
    </div>
  </div>
</div>

@endsection


@section('script')
<script>
  $(document).ready(function() {
    $.ajax({
      url: "{{ route('api.product.vote.result') }}",
      method: "POST",
      dataType: 'json',
      data: {
        _token: "{{ csrf_token() }}",
        product: "{{ $product->slug }}",
        ipaddress : "{{ $ipAddress}}"
      }
    }).done(function(response) {

      if (response.status) {
        if (!response.data.vote) {
          $("#likePercent").html(response.data.like + '%');
          $("#dislikePercent").html(response.data.dislike + '%');
          $('.voteButton').html('');
        }

      } else {
        alert(response.message);
      }

    })
  });

  $(document).on('click', "a.vote", function() {

    let type = $(this).attr("actionType");
    $.ajax({
      url: "{{ route('api.product.vote') }}",
      method: "POST",
      dataType: 'json',
      data: {
        _token: "{{ csrf_token() }}",
        product: "{{ $product->slug }}",
        type: type,
        ipaddress : "{{ $ipAddress }}"
      }
    }).done(function(response) {
      if (response.status) {
        $("#likePercent").html(response.data.like);
        $("#dislikePercent").html(response.data.dislike);
        $(".voteButton").html('');
      } else {
        alert(response.message);
      }

    })
  });
</Script>

@endsection