<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    ProductController,
};

use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('socket/product/{slug}', [ProductController::class, 'socketProduct'])->name('socket.product');

Route::resource('product', ProductController::class)->parameters([
    'product' => 'product:slug',
])->only(['show']);

Route::get('/clear-cache-all', function() {

    Artisan::call('cache:clear');

    dd("Cache Clear All ");

});

