<?php

use App\Http\Controllers\Api\VoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['as'=>'api.'], function(){

    Route::post('product/vote', [VoteController::class, 'productVote'])->name('product.vote');
    Route::post('product/vote/result', [VoteController::class, 'productVoteResult'])->name('product.vote.result');

});


