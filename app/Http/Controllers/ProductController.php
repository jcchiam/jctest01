<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie ;

class ProductController extends Controller
{

    protected  $type = ['like', 'dislike'] ;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Product $product)
    {      

        $ipAddress = $request->ip();
        return view('product', compact('product','ipAddress'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function socketProduct(Request $request, $slug)
    {   

        $product = Product::where('slug', $slug)->first();

        $ipAddress =  str_replace( array( '.', '::'  ), '_' ,$request->ip() );   ;

        if(empty($product)){
            abort(404);
        }

        $voteCookie = isset($_COOKIE[$ipAddress]) ;

        if($voteCookie == false ){
            $vote = true ;
        }
        else {
            $vote = in_array($_COOKIE[$ipAddress],  $this->type) ?  false :  true ;
        };
        
        return view('socketProduct', compact('product','ipAddress', 'vote'));
    }

}
