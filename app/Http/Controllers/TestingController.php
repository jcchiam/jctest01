<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Testing;

class TestingController extends Controller
{
    public function index() {

        $tests = Testing::all();

        return view ('welcome', compact('tests'));
    }
}
