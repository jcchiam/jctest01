<?php

namespace App\Http\Controllers\Api;

use App\Models\Vote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\Cookie;

class VoteController extends Controller
{


    protected  $type = ['like', 'dislike'] ;
    protected $expiryTime = 120;
    /**
     * Display a listing of the resource.s
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function show(Vote $vote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function edit(Vote $vote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vote $vote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vote $vote)
    {
        //
    }


    public function productVoteResult(Request $request) {

        try {

            $ipaddress = str_replace( array( '.', '::'  ), '_' ,$request->ip() );  

            $productVerify = Product::where('slug', $request->get('product'))->first();

            $like = 0 ;
            $dislike = 0 ;

            $vote = false ;  // unable to yet

            if(!empty($productVerify) && $productVerify->like != 0 && $productVerify->dislike != 0 ){
                
                $total = $productVerify->like + $productVerify->dislike ;

                $like =   round( ( $productVerify->like / $total) * 100 , 2) ;
                $dislike = round( ( $productVerify->dislike / $total) * 100 , 2) ;
            }

            $voteCookie = isset($_COOKIE[$ipaddress]) ;

            if($voteCookie == false ){
                
                $vote = true ; 

                return response()->json([
                    'status' => true ,
                    'code' => 200 ,
                    'message' => "Product Vote Result",
                    'data' => [
                        'like' => $like ,
                        'dislike' => $dislike,
                        'vote' => $vote
                    ]
                ])->withCookie(cookie($ipaddress, 'firstVisit', $this->expiryTime));

                
            } 
            else { 

                in_array($_COOKIE[$ipaddress],  $this->type) ? $vote = false : $vote = true ;
                
                return response()->json([
                    'status' => true ,
                    'code' => 200 ,
                    'message' => "Product Vote Result",
                    'data' => [
                        'like' => $like ,
                        'dislike' => $dislike,
                        'vote' => $vote
                    ]
                ]);
            }
        
        } catch (Exception $e) {

            return  response()->json([
                'status' => false ,
                'code' => 501 ,
                'message' =>  $e->getMessage(),
            
            ]);    
        }
    }

    public function productVote(Request $request) {


        try { 

            if (!in_array($request->get('type'),  $this->type)) {
               throw new Exception("Invalid Request");
            }
            
            $productVerify = Product::where('slug', $request->get('product'))->first();

            if(empty($productVerify)){
                throw new Exception("Product Not Found");
            }
         
            if($request->get('type') == 'like'){
                $productVerify->like =  $productVerify->like + 1 ;
                $productVerify->save();
            }

            if($request->get('type') == 'dislike'){
                $productVerify->dislike =  $productVerify->dislike + 1 ;
                $productVerify->save();
            }

            $total = $productVerify->like + $productVerify->dislike ;
            $like =   round( ( $productVerify->like / $total) * 100 , 2) ;
            $dislike = round( ( $productVerify->dislike / $total) * 100 , 2) ;

            $ipaddress =  str_replace( array( '.', '::'  ), '_' , $request->get('ipaddress') );

            return response()->json([
                'status' => true ,
                'code' => 200 ,
                'message' => "Product Vote Result",
                'data' => [
                    'like' => $like ,
                    'dislike' => $dislike
                ]
            ])->withCookie(cookie($ipaddress, $request->get('type'), $this->expiryTime));
            
            
        } catch (Exception $e) {

            return  response()->json([
                'status' => false ,
                'code' => 501 ,
                'message' =>  $e->getMessage(),
            
            ]);    
        }

    
    }
}
