const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    cors: { origin: "*"}
});
const axios = require('axios').default;
const env = require('dotenv').config();

io.on('connection',(socket)=>{
    console.log('connection');

    socket.on('getProductVoteResult', async (product)=>{

        let dataRespond ;

        let data = {
            product : product.product ,
            ipaddress: product.ipaddress ,
        }

        var domain =  env.parsed.APP_URL;

        const resp = await axios.post(domain + '/api/product/vote/result', data);
  
        dataRespond = {
            "like" : resp.data.data['like'] ,
            "dislike" : resp.data.data['dislike'] 
        };

        io.sockets.emit('showProductVoteResult', dataRespond);

    });

    socket.on('disconnect', (socket)=> {
        console.log('disconnect');
    });
});

server.listen(3000,() => {
    console.log('server is running');
})